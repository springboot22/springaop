package com.example1;

import org.springframework.stereotype.Service;

/**
 * @author will.tuo
 * @date 2021/8/12 13:37
 */

@Service
public class StudentService {

    public void add() {
        System.out.println("添加student");
    }

    @Mylog(level="debug")
    public Object update(String name) {
        System.out.println("更新student");
        return "更新成功";
    }
}
