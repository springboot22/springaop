package com.example1;

import org.springframework.stereotype.Service;

/**
 * @author will.tuo
 * @date 2021/8/12 13:36
 */
@Service
public class UserService {

    @Mylog(level="debug")
    public void add() {
        System.out.println("添加用户");
    }

    @Mylog
    public void find() {
        System.out.println("查看用户");
    }
}