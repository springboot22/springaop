package com.example1;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

/**
 * @author will.tuo
 * @date 2021/8/12 13:40
 */
@Configuration
@ComponentScan("com.example1")
@EnableAspectJAutoProxy//启动Spring对AspectJ的支持
public class JavaConfig {

}
