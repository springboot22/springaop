package com.example1;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * @author will.tuo
 * @date 2021/8/12 13:41
 */
public class Main {
    public static void main(String[] args) {
        AnnotationConfigApplicationContext ac = new AnnotationConfigApplicationContext(JavaConfig.class);
        //ApplicationContext ac1 = new ApplicationContext();
        UserService us = ac.getBean(UserService.class);
        us.add();
        us.find();
        System.out.println("------------------------");
        StudentService ss = ac.getBean(StudentService.class);
        ss.add();
        Object ob = ss.update("zhangsan");
        System.out.println(ob);
        System.out.println("------------------------");
        Object ob2 = ss.update("lisi");
        System.out.println(ob2);
        ac.close();
    }
}
