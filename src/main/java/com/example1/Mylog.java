package com.example1;

import java.lang.annotation.*;

/**
 * @author will.tuo
 * @date 2021/8/12 13:36
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Mylog {
    String level() default "info";
}